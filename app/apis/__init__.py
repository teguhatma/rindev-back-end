from flask import Blueprint

bp = Blueprint("apis", __name__, template_folder="templates")

from .routes import (
    company_profile,
    roles,
    position,
    contact,
    portfolio,
    auth,
    user,
)
