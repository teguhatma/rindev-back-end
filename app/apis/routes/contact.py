from flask import jsonify, request, current_app
from app.models import models
from app import db
from .. import bp as apis
from app.decorators import permission_required
from flask_jwt_extended import jwt_required
from app.email.email import send_contact_form

contact_schema = models.Contact_Schema()
contact_schemas = models.Contact_Schema(many=True)


@apis.route("/api/contacts/")
@jwt_required
@permission_required(["Developer", "Administrator"])
def contacts():
    data = models.Contact.query.all()
    return jsonify(contact_schemas.dump(data))


@apis.route("/api/contact/<int:id>", methods=["GET"])
@jwt_required
@permission_required(["Developer", "Administrator"])
def contact_detail(id):
    data = models.Contact.query.get(id)
    return contact_schema.jsonify(data)


@apis.route("/api/contact/", methods=["POST"])
def create_contact():
    name = request.json.get("name")
    email = request.json.get("email")
    subject = request.json.get("subject")
    message = request.json.get("message")

    data = models.Contact(
        name=name,
        email=email,
        subject=subject,
        message=message,
    )
    send_contact_form(data)
    db.session.add(data)
    db.session.commit()
    return jsonify({"msg": "Email sended"}), 200


@apis.route("/api/contact/<int:id>/", methods=["DELETE"])
@jwt_required
@permission_required(["Developer", "Administrator"])
def delete_contact(id):
    data = models.Contact.query.get(id)
    db.session.delete(data)
    db.session.commit()
    return contact_schema.jsonify(data)