from flask import jsonify, request
from app.models import models
from app import db
from .. import bp as apis
from app.decorators import permission_required
from flask_jwt_extended import jwt_required
from datetime import datetime

position_schema = models.Position_Schema()
position_schemas = models.Position_Schema(many=True)


@apis.route("/api/positions/")
@jwt_required
@permission_required(["Developer", "Administrator"])
def positions():
    data = models.Position.query.all()
    return jsonify(position_schemas.dump(data))


@apis.route("/api/position/", methods=["POST"])
@jwt_required
@permission_required(["Developer", "Administrator"])
def create_position():
    data = models.Position(
        name=request.json.get("name"),
    )
    db.session.add(data)
    db.session.commit()
    return position_schema.jsonify(data)


@apis.route("/api/position/<int:id>/", methods=["PATCH"])
@jwt_required
@permission_required(["Developer", "Administrator"])
def update_position(id):
    name = request.json.get("name")

    data = models.Position.query.get(id)

    data.name = name
    data.updated_at = datetime.utcnow()

    db.session.add(data)
    db.session.commit()
    return position_schema.jsonify(data)


@apis.route("/api/position/<int:id>/", methods=["DELETE"])
@jwt_required
@permission_required(["Developer", "Administrator"])
def delete_position(id):
    data = models.Position.query.get(id)
    db.session.delete(data)
    db.session.commit()
    return position_schema.jsonify(data)