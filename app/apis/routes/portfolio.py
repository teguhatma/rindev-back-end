from .. import bp as apis
from app import db
from flask import request, jsonify
from app.models import models
from flask_jwt_extended import get_jwt_identity, jwt_required
from datetime import datetime

portfolio_schema = models.Portfolio_Schema()
portfolio_schemas = models.Portfolio_Schema(
    many=True,
    only=(
        "id",
        "title",
        "description",
        "category",
        "dev_names",
        "technology",
        "link",
        "slug",
    ),
)
detail_portfolio = models.Detail_Portfolio_Schema()


"""
    Api for member Consume
"""


@apis.route("/api/portfolios/", methods=["GET"])
def portfolios():
    data = models.Portfolio.query.all()
    return jsonify(portfolio_schemas.dump(data))


@apis.route("/api/portfolio/", methods=["POST"])
@jwt_required
def create_portfolio():
    user = models.User.query.filter_by(email=get_jwt_identity()).first_or_404()

    title = request.json.get("title")
    description = request.json.get("description")
    category = request.json.get("category")
    technology = request.json.get("technology")
    link = request.json.get("link")
    photo = request.json.get("photo")
    photo_name = request.json.get("photo_name")

    data = models.Portfolio(
        title=title,
        description=description,
        category=category,
        technology=technology,
        link=link,
        user_id=user.id,
        photo=photo,
        photo_name=photo_name,
    )
    db.session.add(data)
    db.session.commit()
    return portfolio_schema.jsonify(data)


@apis.route("/api/portfolio/<int:id>/", methods=["DELETE"])
@jwt_required
def delete_portfolio(id):
    data = models.Portfolio.query.get(id)
    db.session.delete(data)
    db.session.commit()
    return portfolio_schema.jsonify(data)


@apis.route("/api/portfolio/<int:id>", methods=["PATCH"])
@jwt_required
def update_portfolio():
    user = models.User.query.filter_by(email=get_jwt_identity()).first_or_404()

    title = request.json.get("title")
    description = request.json.get("description")
    category = request.json.get("category")
    technology = request.json.get("technology")
    link = request.json.get("link")
    photo = request.json.get("photo")
    photo_name = request.json.get("photo_name")

    data = models.Portfolio.query.get(id)

    data.title = title
    data.description = description
    data.category = category
    data.technology = technology
    data.link = link
    data.updated_at = datetime.utcnow()
    data.user_id = user.id
    data.photo = photo
    data.photo_name = photo_name

    db.session.add(data)
    db.session.commit()
    return portfolio_schema.jsonify(data)


"""
    Api for visitor Consume
"""


@apis.route("/api/portfolio/<slug>/", methods=["GET"])
def detail_portfolio(slug):
    data = models.Portfolio.query.filter_by(slug=slug).first()
    if data is not None:
        return portfolio_schema.jsonify(data)
    else:
        return jsonify({"msg": "Not Found"}), 404
