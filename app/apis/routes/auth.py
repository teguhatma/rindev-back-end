from flask import jsonify, request
from flask_jwt_extended import jwt_required, create_access_token, get_jwt_identity
from .. import bp as apis
from app.models import models
from app import db


@apis.route("/api/login/", methods=["POST"])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    email = request.json.get("email")
    password = request.json.get("password")

    data = models.User.query.filter_by(email=email).first()

    if data is None or not data.check_password(password):
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=email)
    return jsonify(token=access_token), 200


@apis.route("/api/set-password/<token>/", methods=["POST"])
def set_password(token):
    user = User.verify_password_token(token)
    if not user:
        return jsonify({"msg": "Token has expired."}), 401

    password = request.json.get("password")

    user.password(password)
    db.session.add(user)
    db.session.commit()
    return jsonify({"msg": "Password has changed"}), 200
