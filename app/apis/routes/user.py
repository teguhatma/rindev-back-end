from app import db
from app.models import models
from flask import jsonify, request
from .. import bp as apis
from app.decorators import permission_required
from flask_jwt_extended import jwt_required, get_jwt_identity
from app.email.email import send_set_password
from datetime import datetime

user_schema = models.User_Schema()
user_schemas = models.User_Limit(many=True)


@apis.route("/api/users/")
@jwt_required
def users():
    data = models.User.query.all()
    return jsonify(user_schemas.dump(data))


@apis.route("/api/user/", methods=["POST"])
@jwt_required
@permission_required(["Developer"])
def create_user():
    name = request.json.get("name")
    username = request.json.get("username")
    email = request.json.get("email")
    photo = request.json.get("photo")
    photo_name = request.json.get("photo_name")
    master = request.json.get("master")
    role = request.json.get("role")
    position = request.json.get("position")

    roles = models.Role.query.filter_by(name=role).first()

    data = models.User(
        name=name,
        username=username,
        email=email,
        photo=photo,
        photo_name=photo_name,
        roles=[roles],
        position_id=int(position),
        master=master,
    )

    send_set_password(data)

    db.session.add(data)
    db.session.commit()
    return user_schema.jsonify(data)


@apis.route("/api/user/<int:id>/", methods=["DELETE"])
@jwt_required
@permission_required(["Developer"])
def delete_user(id):
    data = models.User.query.get(id)

    db.session.delete(data)
    db.session.commit()
    return user_schema.jsonify(data)


@apis.route("/api/user/<int:id>/", methods=["GET"])
@jwt_required
@permission_required(["Developer"])
def detail_user(id):
    data = models.User.query.get(id)
    return user_schema.dump(data)


@apis.route("/api/user/<int:id>/", methods=["PATCH"])
@jwt_required
@permission_required(["Developer"])
def update_user(id):
    name = request.json.get("name")
    username = request.json.get("username")
    email = request.json.get("email")
    photo = request.json.get("photo")
    photo_name = request.json.get("photo_name")
    role = request.json.get("role")
    position = request.json.get("position")
    master = request.json.get("master")

    roles = models.Role.query.filter_by(name=role).first()
    data = models.User.query.get(id)

    data.name = name
    data.username = username
    data.email = email
    data.photo = photo
    data.photo_name = photo_name
    data.roles = [roles]
    data.position_id = int(position)
    data.updated_at = datetime.utcnow()
    data.master = master

    db.session.add(data)
    db.session.commit()
    return user_schema.jsonify(data)