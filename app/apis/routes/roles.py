from app import db
from app.models import models
from flask import jsonify, request
from .. import bp as apis
from app.decorators import permission_required
from flask_jwt_extended import jwt_required
from datetime import datetime

role_schema = models.Role_Schema()
role_schemas = models.Role_Schema(many=True)


@apis.route("/api/roles/")
def roles():
    data = models.Role.query.all()
    return jsonify(role_schemas.dump(data))


@apis.route("/api/role/", methods=["POST"])
@jwt_required
@permission_required(["Developer"])
def create_role():
    data = models.Role(name=request.json.get("name"))
    db.session.add(data)
    db.session.commit()
    return jsonify(role_schema.dump(data))


@apis.route("/api/role/<int:id>/", methods=["GET"])
@jwt_required
@permission_required(["Developer"])
def role_detail(id):
    data = models.Role.query.get(id)
    return role_schema.jsonify(data)


@apis.route("/api/role/<int:id>/", methods=["PATCH"])
@jwt_required
@permission_required(["Developer"])
def update_role(id):
    name = request.json.get("name")

    data = models.Role.query.get(id)

    data.name = name
    data.updated_at = datetime.utcnow()

    db.session.add(data)
    db.session.commit()
    return role_schema.jsonify(data)


@apis.route("/api/role/<int:id>/", methods=["DELETE"])
@jwt_required
@permission_required(["Developer"])
def delete_role(id):
    data = models.Role.query.get(id)
    db.session.delete(data)
    db.session.commit()
    return role_schema.jsonify(data)