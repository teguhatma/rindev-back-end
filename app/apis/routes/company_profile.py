from app import db
from app.models import models
from flask import jsonify, request
from .. import bp as apis
from app.decorators import permission_required
from flask_jwt_extended import jwt_required
from datetime import datetime

company_profile_schema = models.Company_Profile_Schema()
company_profile_schemas = models.Company_Profile_Schema(
    many=True,
    only=(
        "id",
        "name",
        "company_type",
        "address",
        "province",
        "country",
        "postal_code",
        "email",
        "website",
        "phone_number",
        "instagram",
        "twitter",
        "facebook",
        "description",
    ),
)


@apis.route("/api/company-profile/")
def company_profile():
    data = models.Company_Profile.query.all()
    return jsonify(company_profile_schemas.dump(data))


@apis.route("/api/company-profile/", methods=["POST"])
@jwt_required
@permission_required(["Developer", "Administrator"])
def create_company_profile():
    name = request.json.get("name")
    address = request.json.get("address")
    province = request.json.get("province")
    country = request.json.get("country")
    postal_code = request.json.get("postal_code")
    email = request.json.get("email")
    website = request.json.get("website")
    phone_number = request.json.get("phone_number")
    logo = request.json.get("logo")
    logo_name = request.json.get("logo_name")
    data = models.Company_Profile(
        name=name,
        address=address,
        province=province,
        country=country,
        postal_code=postal_code,
        email=email,
        website=website,
        phone_number=phone_number,
        logo=logo,
        logo_name=logo_name,
        created_at=created_at,
        updated_at=updated_at,
    )
    db.session.add(data)
    db.session.commit()
    return company_profile_schema.jsonify(data)


@apis.route("/api/company-profile/<int:id>/", methods=["GET"])
@jwt_required
@permission_required(["Developer", "Administrator"])
def company_profile_detail(id):
    data = models.Company_Profile.query.get(id)
    return company_profile_schema.jsonify(data)


@apis.route("/api/company-profile/<int:id>/", methods=["PATCH"])
@jwt_required
@permission_required(["Developer", "Administrator"])
def update_company_profile(id):
    name = request.json.get("name")
    address = request.json.get("address")
    province = request.json.get("province")
    country = request.json.get("country")
    postal_code = request.json.get("postal_code")
    email = request.json.get("email")
    website = request.json.get("website")
    phone_number = request.json.get("phone_number")
    logo = request.json.get("logo")
    logo_name = request.json.get("logo_name")

    data = models.Company_Profile.query.get(id)

    data.name = name
    data.address = address
    data.province = province
    data.country = country
    data.postal_code = postal_code
    data.email = email
    data.website = website
    data.phone_number = phone_number
    data.logo = logo
    data.logo_name = logo_name
    data.updated_at = datetime.utcnow()

    db.session.add(data)
    db.session.commit()
    return company_profile_schema.jsonify(data)


@apis.route("/api/company-profile/<int:id>/", methods=["DELETE"])
@jwt_required
@permission_required(["Developer", "Administrator"])
def delete_company_profile(id):
    data = models.Company_Profile.query.get(id)
    db.session.delete(data)
    db.session.commit()
    return company_profile_schema.jsonify(data)