from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask import Flask
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_mail import Mail
from flask_jwt_extended import JWTManager
from flask_login import LoginManager

db = SQLAlchemy()
migrate = Migrate()
mail = Mail()
cors = CORS()
ma = Marshmallow()
jwt = JWTManager()
login_manager = LoginManager()


def create_app(config_class=Config):
    """
    function for create initial app
    """
    app = Flask(__name__)
    app.config.from_object(config_class)
    db.init_app(app)
    migrate.init_app(app, db)
    mail.init_app(app)
    cors.init_app(app)
    ma.init_app(app)
    login_manager.init_app(app)
    jwt.init_app(app)

    from app.apis import bp as apis

    app.register_blueprint(apis)

    from app.views import bp as views

    app.register_blueprint(views)

    return app


from app.models import models