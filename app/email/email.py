from flask import current_app, render_template
from flask_mail import Message
from app import mail
from threading import Thread


def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    Thread(
        target=send_async_email, args=(current_app._get_current_object(), msg)
    ).start()


def send_set_password(user):
    token = user.get_password_token()
    send_email(
        "Set Your Password",
        sender=current_app.config["MAIL_OFFICE"],
        recipients=[user.email],
        text_body=render_template("password.txt", user=user, token=token),
        html_body=render_template("password.html", user=user, token=token),
    )


def send_contact_form(user):
    send_email(
        user.subject,
        sender="rinjanideveloper@gmail.com",
        recipients=["teguhatmayudha@gmail.com"],
        text_body=user.message,
        html_body=user.message,
    )
