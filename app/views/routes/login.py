from flask import render_template, url_for, request, redirect
from app import db
from .. import bp as views
from app.models import models
from flask_security import login_required, login_user, current_user


@views.route("/login", methods=["GET", "POST"])
def views_login():
    if request.method == "POST":
        email = request.form.get("email")
        pwd = request.form.get("password")

        data = models.User.query.filter_by(email=email).first()

        if data is not None:
            if data.check_password(pwd):
                login_user(data)
                return redirect(url_for("views.views_dashboard"))
            else:
                print("Pasw")

    return render_template("auth/login.html")


@views.route("/dashboard")
@login_required
def views_dashboard():
    return render_template("dashboard.html")


@views.route("/dashboard/user", methods=["GET", "POST"])
@login_required
def views_user():
    user = models.User.query.first()
    return render_template("user.html", user=user)


@views.route("/dashboard/company-profile", methods=["GET", "POST"])
@login_required
def views_company_profile():
    data = models.Company_Profile.query.first()
    if request.method == "POST":
        name = request.form.get("name")
        company_type = request.form.get("company_type")
        address = request.form.get("address")
        province = request.form.get("province")
        country = request.form.get("country")
        postal_code = request.form.get("postal_code")
        email = request.form.get("email")
        website = request.form.get("website")
        phone_number = request.form.get("phone")
        description = request.form.get("description")
        instagram = request.form.get("instagram")
        facebook = request.form.get("facebook")
        twitter = request.form.get("twitter")
        photo = request.files["photo"]
        logo = request.files["logo"]

        data.name = name
        data.company_type = company_type
        data.address = address
        data.province = province
        data.country = country
        data.postal_code = postal_code
        data.email = email
        data.website = website
        data.phone_number = phone_number
        data.instagram = instagram
        data.twitter = twitter
        data.facebook = facebook
        data.description = description
        data.image = photo.read()
        data.image_name = photo.filename
        data.logo = logo.read()
        data.logo_name = logo.filename

        db.session.add(data)
        db.session.commit()
        return redirect(url_for(".views_company_profile"))
    return render_template("company_profile.html", company_profile=data)


@views.route("/dashboard/portofolio")
@login_required
def views_portofolio():
    data = models.Portfolio.query.filter_by(user_id=current_user.id).all()
    return render_template("portofolios.html", data=data)


@views.route("/dashboard/tambah-portofolio", methods=["GET", "POST"])
@login_required
def views_tambah_portofolio():
    if request.method == "POST":
        data = models.Portfolio(
            user_id=current_user.id,
            title=request.form.get("title"),
            dev_names=request.form.get("dev_names"),
            category=request.form.get("category"),
            photo=request.files["photo"].read(),
            photo_name=request.files["photo"].filename,
            technology=request.form.get("technology"),
            link=request.form.get("link"),
            description=request.form.get("description"),
        )

        db.session.add(data)
        db.session.commit()
        return redirect(url_for(".views_portofolio"))
    return render_template("tambah_portfolio.html")


@views.route("/dashboard/<id>/edit-portofolio", methods=["GET", "POST"])
@login_required
def views_edit_portofolio(id):
    data = models.Portfolio.query.get(id)

    if request.method == "POST":
        title = request.form.get("title")
        dev_names = request.form.get("dev_names")
        category = request.form.get("category")
        photo = request.files["photo"].read()
        photo_name = request.files["photo"].filename
        technology = request.form.get("technology")
        link = request.form.get("link")
        description = request.form.get("description")

        data.title = title
        data.dev_names = dev_names
        data.category = category
        data.photo = photo
        data.photo_name = photo_name
        data.technology = technology
        data.link = link
        data.description = description

        db.session.add(data)
        db.session.commit()
        return redirect(url_for(".views_portofolio"))

    return render_template("edit_portfolio.html", data=data)


@views.route("/dashboard/<id>/hapus-portofolio", methods=["GET", "POST"])
@login_required
def views_hapus_portofolio(id):
    data = models.Portfolio.query.get(id)
    db.session.delete(id)
    db.session.commit()
    return redirect(url_for(".views_portofolio"))
