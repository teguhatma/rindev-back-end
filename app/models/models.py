from app import db, ma, login_manager
from flask import current_app
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_security import UserMixin, RoleMixin
from time import time
import jwt
from slugify import slugify
from sqlalchemy import event

__photosize__ = 4024


"""
Define Relations Between User and Roles
"""


roles_users = db.Table(
    "roles_users",
    db.Column("user_id", db.Integer(), db.ForeignKey("user.id")),
    db.Column("role_id", db.Integer(), db.ForeignKey("role.id")),
)


"""
Create Table Roles
"""


class Role(db.Model, RoleMixin):
    __tablename__ = "role"

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    created_at = db.Column(db.DateTime())
    updated_at = db.Column(db.DateTime())

    _default_fields = [
        "id",
        "name",
        "created_at",
        "updated_at",
    ]

    @staticmethod
    def check_if_exist():
        """
        Check if data exist or not in table roles
        """
        return Role.query.first()

    def __repr__(self):
        return "Role {}".format(self.name)


"""
Create Table Position
"""


class Position(db.Model):
    __tablename__ = "position"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    created_at = db.Column(db.DateTime())
    updated_at = db.Column(db.DateTime())
    user = db.relationship("User", back_populates="positions")  # Relation to User

    _default_fields = [
        "id",
        "name",
    ]

    def __repr__(self):
        return "Position {}".format(self.name)

    def __init__(self, **kwargs):
        super(Position, self).__init__(**kwargs)
        if self.created_at is None:
            self.created_at = datetime.utcnow()
        if self.updated_at is None:
            self.updated_at = datetime.utcnow()


"""
Create Table Portfolio
"""


class Portfolio(db.Model):
    __tablename__ = "portfolio"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
    photo = db.Column(db.LargeBinary(__photosize__))
    photo_name = db.Column(db.String(124))
    description = db.Column(db.Text())
    category = db.Column(db.String(64))
    dev_names = db.Column(db.String(245))
    technology = db.Column(db.String(124))
    link = db.Column(db.String(124))
    created_at = db.Column(db.DateTime())
    slug = db.Column(db.String(124), unique=True)
    updated_at = db.Column(db.DateTime())
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))  # relation to user
    developer = db.relationship("User", back_populates="portfolio")

    _default_fields = [
        "id",
        "title",
        "photo",
        "photo_name",
        "description",
        "dev_names",
        "slug",
        "category",
        "technology",
        "link",
        "created_at",
        "updated_at",
    ]

    @staticmethod
    def generate_slug(target, value, oldvalue, initiator):
        if value and (not target.slug or value != oldvalue):
            target.slug = slugify(value)

    def __init__(self, **kwargs):
        super(Portfolio, self).__init__(**kwargs)
        if self.created_at is None:
            self.created_at = datetime.utcnow()
        if self.updated_at is None:
            self.updated_at = datetime.utcnow()

    def __repr__(self):
        return "Portfolio {}".format(self.title)


event.listen(Portfolio.title, "set", Portfolio.generate_slug, retval=False)


"""
Create Table User
"""


class User(db.Model, UserMixin):
    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    username = db.Column(db.String(64))
    email = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(124))
    master = db.Column(db.Text())
    active = db.Column(db.Boolean(), default=True)
    photo = db.Column(db.LargeBinary(__photosize__))
    photo_name = db.Column(db.String(124))
    instagram = db.Column(db.String(124))
    github = db.Column(db.String(124))
    linkedin = db.Column(db.String(124))
    created_at = db.Column(db.DateTime())
    updated_at = db.Column(db.DateTime())
    roles = db.relationship(
        "Role", secondary=roles_users, backref=db.backref("users", lazy="dynamic")
    )
    position_id = db.Column(
        db.Integer, db.ForeignKey("position.id")
    )  # relation to position
    positions = db.relationship(
        "Position", back_populates="user"
    )  # Relation to Portfolio
    portfolio = db.relationship(
        "Portfolio", back_populates="developer"
    )  # Relation to Portfolio

    _default_fields = [
        "id",
        "name",
        "username",
        "email",
        "master",
        "instagram",
        "github",
        "linkedin",
        "active",
        "photo",
        "photo_name",
        "created_at",
        "updated_at",
    ]

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.created_at is None:
            self.created_at = datetime.utcnow()
        if self.updated_at is None:
            self.updated_at = datetime.utcnow()

    def __repr__(self):
        return "User {}".format(self.name)

    def set_password(self, password):
        self.password = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password, password)

    def get_id(self):
        return self.id

    def get_password_token(self, expires_in=660):
        return jwt.encode(
            {"set_password": self.id, "exp": time() + expires_in},
            current_app.config["SECRET_KEY"],
            algorithm="HS256",
        ).decode("utf-8")

    @staticmethod
    def verify_password_token(token):
        try:
            id = jwt.decode(
                token, current_app.config["SECRET_KEY"], algorithms=["HS256"]
            )["set_password"]
        except:
            return
        return User.query.get(id)


"""
Create Table Company Profile
"""


class Company_Profile(db.Model):
    __tablename__ = "company_profile"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    company_type = db.Column(db.String(24))
    address = db.Column(db.Text())
    province = db.Column(db.String(64))
    country = db.Column(db.String(64))
    postal_code = db.Column(db.String(12))
    email = db.Column(db.String(64))
    website = db.Column(db.String(64))
    phone_number = db.Column(db.String(24))
    instagram = db.Column(db.String(124))
    twitter = db.Column(db.String(124))
    facebook = db.Column(db.String(124))
    image = db.Column(db.LargeBinary(__photosize__))
    image_name = db.Column(db.String(124))
    logo = db.Column(db.LargeBinary(__photosize__))
    logo_name = db.Column(db.String(124))
    description = db.Column(db.Text())
    created_at = db.Column(db.DateTime())
    updated_at = db.Column(db.DateTime())

    _default_fields = [
        "id",
        "name",
        "address",
        "province",
        "country",
        "postal_code",
        "email",
        "phone_number",
        "instagram",
        "facebook",
        "twitter",
        "company_type",
        "image",
        "image_name",
        "logo",
        "logo_name",
        "description",
        "created_at",
        "updated_at",
    ]

    def __init__(self, **kwargs):
        super(Company_Profile, self).__init__(**kwargs)
        if self.created_at is None:
            self.created_at = datetime.utcnow()
        if self.updated_at is None:
            self.updated_at = datetime.utcnow()

    def __repr__(self):
        return "Company Name {}".format(self.name)


"""
Create Table Contact
"""


class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(62))
    email = db.Column(db.String(64))
    subject = db.Column(db.String(64))
    message = db.Column(db.Text())
    timestamp = db.Column(db.DateTime())

    _default_fields = [
        "id",
        "name",
        "email",
        "subject",
        "message",
    ]

    def __init__(self, **kwargs):
        super(Contact, self).__init__(**kwargs)
        if self.timestamp is None:
            self.timestamp = datetime.utcnow()

    def __repr__(self):
        return "Contact {}".format(self.name)


"""
Create Schema Company Profile
"""


class Company_Profile_Schema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Company_Profile


"""
Create Schema Role
"""


class Role_Schema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Role


"""
Create Schema Position
"""


class Position_Schema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Position


"""
Create Schema Contact
"""


class Contact_Schema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Contact


"""
Create Schema Portfolio
"""


class Portfolio_Schema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Portfolio


"""
Create Schema User
"""


class User_Schema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User


class User_Limit(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        fields = (
            "id",
            "name",
            "email",
            "active",
            "roles",
            "positions",
        )

    roles = ma.Nested(Role_Schema, many=True, only=("name",))
    positions = ma.Nested(Position_Schema, only=("name",))


"""
Create Schema for Visitor
"""


class Detail_Portfolio_Schema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Portfolio

    developer = ma.Nested(
        User_Schema,
        only=(
            "name",
            "master",
        ),
    )


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)