from functools import wraps
from flask import abort
from app.models import models


def permission_required(permission):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            current_user = models.User.query.filter_by(
                email="teguhatmayudha@gmail.com"
            ).first()
            for role in current_user.roles:
                if not role.name in permission:
                    abort(403)
            return f(*args, **kwargs)

        return decorated_function

    return decorator