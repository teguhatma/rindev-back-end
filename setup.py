from app import create_app, db
from app.models.models import User, Role
from flask_security import SQLAlchemyUserDatastore, Security
from datetime import datetime

app = create_app()
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)


@app.shell_context_processor
def make_shell_context():
    return {"db": db}


@app.cli.command()
def deploy():
    """
    Insert Default Roles data only if Role table is empty.
    """
    if Role.check_if_exist():
        user = user_datastore.create_role(
            name="User",
            updated_at=datetime.utcnow(),
            created_at=datetime.utcnow(),
        )
        admin = user_datastore.create_role(
            name="Admin",
            updated_at=datetime.utcnow(),
            created_at=datetime.utcnow(),
        )
        developer = user_datastore.create_role(
            name="Developer",
            updated_at=datetime.utcnow(),
            created_at=datetime.utcnow(),
        )
        db.session.commit()