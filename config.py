# settings.py
from os.path import join, dirname
from dotenv import load_dotenv
import os

# Create .env file path.
dotenv_path = join(dirname(__file__), ".env")

# Load file from the path.
load_dotenv(dotenv_path)


class Config(object):
    """
    Configuration system
    """

    APP_NAME = os.getenv("APP_NAME")
    SECRET_KEY = os.getenv("SECRET_KEY") or "let-me-in-please"
    LOG_TO_STDOUT = os.getenv("LOG_TO_STDOUT")
    FLASK_ENV = os.getenv("FLASK_ENV")
    FLASK_DEBUG = os.getenv("FLASK_DEBUG")
    FLASK_APP = os.getenv("FLASK_APP")

    # SQLAlchemy
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URL")
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv("SQLALCHEMY_TRACK_MODIFICATIONS")
    # Email config
    MAIL_SERVER = os.getenv("MAIL_SERVER")
    MAIL_PORT = os.getenv("MAIL_PORT")
    MAIL_USE_SSL = os.getenv("MAIL_USE_SSL")
    MAIL_USERNAME = os.getenv("MAIL_USERNAME")
    MAIL_PASSWORD = os.getenv("MAIL_PASSWORD")
    MAIL_DEVELOPER = os.getenv("MAIL_DEVELOPER")
    MAIL_OFFICE = os.getenv("MAIL_OFFICE")

    CORS_RESOURCES = {
        r"/api/*": {
            "origins": [
                "https://rinjanideveloper.netlify.app",
                "https://rinjanideveloper.com",
                "https://www.rinjanideveloper.com",
            ]
        }
    }
    CORS_HEADER = "Content-Type"

    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
    SECURITY_POST_LOGIN_VIEW = "/dashboard"
