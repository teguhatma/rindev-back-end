# Development Setup

Requirements: Python 3.8, Internet Access

## Starting Development Server

Open terminal/bash and navigat to the directory where you want the project to live.

### Manual installation Setup

Clone this repository

```bash
$ git clone git@gitlab.com:teguhatma/rindev-back-end.git
```

Navigate to the newly cloned repo

```bash
$ cd rindev-back-end
```

Create your local environtment

```bash
$ python -m venv env
```

Activate your local environtment

```bash
$ . env/bin/activate
```

Install packages that requires for this projects

```bash
$ pip install -r requirements.txt
```

Copy example environment file & add your credentials settings (SECRET_KEY, DB, etc)

```bash
$ cp .env.example .env
```

### Data Preparation

1. Doing initilization migration

```bash
$ flask db init
```

2. Doing migration

```bash
$ flask db migrate -m "initial migration"
```

3. Apply migration

```bash
$ flask db upgrade
```

### Running application

After all set, let's running our development server

```bash
$ flask run -p 8080
```

### Running local mail server (Testing Purpose Only)

If you want to testing email with localserver make sure setting you several .env variabel to :

```
...
FLASK_DEBUG=0
MAIL_SERVER=localhost
MAIL_PORT=8025
MAIL_USE_TLS=
MAIL_USERNAME=
MAIL_PASSWORD=
...
```

And then run the local mail server with this command :

```bash
$ python -m smtpd -n -c DebuggingServer localhost:8025
```

# Problems

Several problem will showing with several command

### Error when create migration

```bash
$ flask db migrate -m "messages"
ERROR [root] Error: Can't locate revision identified by '89ed8ff874ae'
```

### Solution

```bash
$ flask db revision --rev-id 89ed8ff874ae
$ flask db migrate -m "messages"
$ flask db upgrade
```

# License

The computer software is licensed under the Apache License 2.0 license.

# Contributors

- [teguhatma](https://gitlab.com/teguhatma)
